import subprocess
import urlparse
import pymongo

import scrapy
from moviepy.editor import VideoFileClip

from scrapy.exceptions import DropItem
from scrapy.pipelines.images import FilesPipeline
from .settings import FILES_STORE


class MongoPipeline(object):

    collection_name = 'hyp_video_items'

    def __init__(self, mongo_uri, mongo_db):
        self.mongo_uri = mongo_uri
        self.mongo_db = mongo_db

    @classmethod
    def from_crawler(cls, crawler):
        return cls(
            mongo_uri=crawler.settings.get('MONGO_URI'),
            mongo_db=crawler.settings.get('MONGO_DATABASE', 'items')
        )

    def open_spider(self, spider):
        self.client = pymongo.MongoClient(self.mongo_uri)
        self.db = self.client[self.mongo_db]

    def close_spider(self, spider):
        self.client.close()

    def process_item(self, item, spider):
        self.db[self.collection_name].insert_one(dict(item))
        return item


class DuplicatesPipeline(object):

    collection_name = 'hyp_video_items'

    def __init__(self, mongo_uri, mongo_db):
        self.mongo_uri = mongo_uri
        self.mongo_db = mongo_db

    @classmethod
    def from_crawler(cls, crawler):
        return cls(
            mongo_uri=crawler.settings.get('MONGO_URI'),
            mongo_db=crawler.settings.get('MONGO_DATABASE', 'items')
        )

    def open_spider(self, spider):
        self.client = pymongo.MongoClient(self.mongo_uri)
        self.db = self.client[self.mongo_db]

    def close_spider(self, spider):
        self.client.close()

    def process_item(self, item, spider):
        if self.db[self.collection_name].find_one(dict(id=item['id'])):
            raise DropItem("Duplicate item found: %s" % item)
        return item


class FileDownloadPipeline(FilesPipeline):

    def file_path(self, request, response=None, info=None):
        # item=request.meta['item'] # Like this you can use all from item, not just url.
        url_params = dict(urlparse.parse_qsl(urlparse.urlsplit(
            request.url).query))
        video_id = url_params['video_id']
        return 'videos/{0}.mp4'.format(video_id)

    def get_media_requests(self, item, info):
        yield scrapy.Request(item['url'])

    def item_completed(self, results, item, info):
        # self._resolution_maintain(item)
        return item

    def _resolution_maintain(self, item):
        file_absolute_path = self.get_full_path(item)
        video = VideoFileClip(file_absolute_path)
        if video.aspect_ratio < 0.75:
            original_width, original_height = video.size
            new_width, new_height = original_width, int(original_width/0.75)
            height_to_cut = original_height - new_height
            y = int((height_to_cut*2.0)/5.0)
            crop_str = 'crop={w}:{h}:{x}:{y}'.format(
                w=new_width, h=new_height, x=0, y=y)
            crop_file_path = self.get_crop_full_path(item)
            subprocess.call(
                ['ffmpeg', '-i', file_absolute_path, '-filter:v',
                 crop_str, crop_file_path, '-y'])

    @staticmethod
    def get_full_path(item):
        return '{0}videos/{1}.mp4'.format(FILES_STORE, item['id'])

    @staticmethod
    def get_crop_full_path(item):
        return '{0}crop_videos/{1}_crop.mp4'.format(FILES_STORE, item['id'])




