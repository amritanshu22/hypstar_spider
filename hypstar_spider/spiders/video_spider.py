import json
import logging
import scrapy
import urlparse
import urllib

from hypstar_spider.items import VideoItem


BASE_URL = 'https://api.hypstar.com/hotsoon/feed/'


class VideoSpider(scrapy.Spider):
    name = 'videos'

    start_urls = [
        'https://api.hypstar.com/hotsoon/feed/'
        '?type=video&min_time=0&offset=0&count=10'
        '&req_from=feed_refresh&iid=6530167078714869505&aid=1145'
    ]

    def parse(self, response):
        resp_dict = None
        try:
            resp_dict = json.loads(response.body)
        except ValueError:
            logging.warning('Unable to parse {0}'.format(response.body))

        if resp_dict:
            video_dicts = resp_dict['data']
            for video_dict in video_dicts:
                video_item = VideoItem(
                    id=video_dict['data']['video']['video_id'],
                    url=video_dict['data']['video']['download_url'][1])
                yield video_item

            if resp_dict['extra']['has_more']:
                params = {
                    'offset': 0,
                    'count': 20,
                    'req_from': 'feed_loadmore',
                    'iid': '6530167078714869505',
                    'aid': 1145,
                    'type': 'video',
                    'max_time': resp_dict['extra']['max_time']
                }
                new_url = BASE_URL + '?' + urllib.urlencode(params)
                yield scrapy.Request(new_url, callback=self.parse)

